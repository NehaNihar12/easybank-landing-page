function showNav(){
    const hamburger = document.querySelector('.hamburger');
    const nav_menu = document.querySelector('.nav-menu');
    const bg_top = document.querySelector('.bg-top');
    const header = document.querySelector('.header');
    var Image_Id = document.getElementById('imgClickAndChange');
    hamburger.addEventListener('click',()=>{
        nav_menu.classList.toggle('active');
        bg_top.classList.toggle('active');
        header.classList.toggle('active');

        if (Image_Id.src.match("./images/icon-hamburger.svg")) {
            Image_Id.src = "./images/icon-close.svg";
            
        }
        else {
            Image_Id.src = "./images/icon-hamburger.svg";
        }
    })

    document.querySelectorAll('.nav-link')
    .forEach(n=>n.addEventListener('click',
    ()=>{
        hamburger.classList.remove('active');
        nav_menu.classList.remove('active');
        bg_top.classList.remove('active');
        header.classList.remove('active');
        Image_Id.src = "./images/icon-hamburger.svg";
    }))
}